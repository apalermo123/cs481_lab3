import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      //darkTheme: ThemeData.light(),
      //theme: ThemeData(brightness: Brightness.light),

      title: 'Lab 3',

      home: new MyHomePage(title: 'Lab 3'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int indexSelected;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
          children: <Widget>[
            AppBar(
              title: Text('Lab 3'),
              actions: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: InputChip(
                      avatar: CircleAvatar(
                        backgroundColor: Colors.grey.shade800,
                        child: Text('AP'),
                      ),
                      label: Text('Anthony Palermo'),
                      backgroundColor: Colors.white70,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => new ProfilePage()),
                      );
                    },
                  )
                ),
              ],
              backgroundColor: Colors.blueGrey,
            ),
            Card(
              shape: BeveledRectangleBorder( borderRadius: BorderRadius.circular(20.0)),
              elevation: 0.8,
              borderOnForeground: true,
              color: Colors.white70,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.airplanemode_active),
                    title: Text('SAN --> LAX', textDirection: TextDirection.ltr, style: TextStyle(fontWeight: FontWeight.bold)),
                    subtitle: Text('Duration: 48 mins', textDirection: TextDirection.ltr),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('7am'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 0,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 0 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('12pm'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 1,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 1 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('5pm'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 2,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 2 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      FlatButton(
                        child: const Text('BUY TICKETS'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => new CheckoutPage()),
                            );
                        },
                      ),
                      const SizedBox(width: 8),
                    ],
                  ),
                ],
              ),
            ),
            Card(
              shape: BeveledRectangleBorder( borderRadius: BorderRadius.circular(20.0)),
              elevation: 0.8,
              borderOnForeground: true,
              color: Colors.white70,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.airplanemode_active),
                    title: Text('LAX --> SAN', textDirection: TextDirection.ltr, style: TextStyle(fontWeight: FontWeight.bold)),
                    subtitle: Text('Duration: 50 mins', textDirection: TextDirection.ltr),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('7am'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 3,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 3 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('12pm'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 4,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 4 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('5pm'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 5,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 5 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      FlatButton(
                        child: const Text('BUY TICKETS'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => new CheckoutPage()),
                          );
                        },
                      ),
                      const SizedBox(width: 8),
                    ],
                  ),
                ],
              ),
            ),
            Card(
              shape: BeveledRectangleBorder( borderRadius: BorderRadius.circular(20.0)),
              elevation: 0.8,
              borderOnForeground: true,
              color: Colors.white70,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.airplanemode_active),
                    title: Text('SAN --> SFO', textDirection: TextDirection.ltr, style: TextStyle(fontWeight: FontWeight.bold)),
                    subtitle: Text('Duration: 1 hour 30 mins', textDirection: TextDirection.ltr),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('7am'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 6,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 6 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('12pm'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 7,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 7 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('5pm'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 8,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 8 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      FlatButton(
                        child: const Text('BUY TICKETS'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => new CheckoutPage()),
                          );
                        },
                      ),
                      const SizedBox(width: 8),
                    ],
                  ),
                ],
              ),
            ),
            Card(
              shape: BeveledRectangleBorder( borderRadius: BorderRadius.circular(20.0)),
              elevation: 0.8,
              borderOnForeground: true,
              color: Colors.white70,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.airplanemode_active),
                    title: Text('SAN --> AUS', textDirection: TextDirection.ltr, style: TextStyle(fontWeight: FontWeight.bold)),
                    subtitle: Text('Duration: 2 hours 45 mins', textDirection: TextDirection.ltr),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('7am'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 9,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 9 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('12pm'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 11,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 11 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      const SizedBox(width: 5),
                      ChoiceChip(
                        label: Text('5pm'),
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        backgroundColor: Colors.white70,
                        elevation: 10.0,
                        pressElevation: 5.0,
                        selected: indexSelected == 10,
                        onSelected: (value) {
                          setState(() {
                            indexSelected = value ? 10 : -1;
                          });
                        },
                        selectedColor: Colors.yellow,
                      ),
                      FlatButton(
                        child: const Text('BUY TICKETS'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => new CheckoutPage()),
                          );
                        },
                      ),
                      const SizedBox(width: 8),
                    ],
                  ),
                ],

              ),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => new FlightDataTables()),
                );
              },
              textColor: Colors.white,
              padding: const EdgeInsets.all(0.0),
              child: Container(
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: <Color>[
                      Colors.blueGrey,
                      Colors.white24,
                    ],
                  ),
                ),
                padding: const EdgeInsets.all(10.0),
                child:
                const Text('Show All Available Flights', style: TextStyle(fontSize: 20, color: Colors.black)),
              ),
            ),
          ],
      ),
    );
  }
}



class FlightDataTables extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text('Flight Data Tables'),
        backgroundColor: Colors.blueGrey,
      ),
      body: ListView(
        padding: const EdgeInsets.all(16),
        children: [
          PaginatedDataTable(
            header: Text('All Available Flights'),
            rowsPerPage: 7,
            columns: [
              DataColumn(label: Text('Origin')),
              DataColumn(label: Text('Destination')),
              DataColumn(label: Text('Time')),
            ],
            source: _DataSource(context),
          ),
        ],
      ),
    );
  }
}

class _Row {
  _Row(
      this.valueA,
      this.valueB,
      this.valueC,
      );
  final String valueA;
  final String valueB;
  final String valueC;
  bool selected = false;
}

class _DataSource extends DataTableSource {
  _DataSource(this.context) {
    _rows = <_Row>[
      _Row('SAN', 'LAX', '7am'),
      _Row('SAN', 'LAX', '12pm'),
      _Row('SAN', 'LAX', '5pm'),
      _Row('LAX', 'SAN', '12pm'),
      _Row('LAX', 'SAN', '5pm'),
      _Row('SAN', 'SFO', '7am'),
      _Row('SAN', 'AUS', '5pm'),
    ];
  }

  final BuildContext context;
  List<_Row> _rows;

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          _selectedCount += value ? 1 : -1;
          assert(_selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
      cells: [
        DataCell(Text(row.valueA)),
        DataCell(Text(row.valueB)),
        DataCell(Text(row.valueC)),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}

class CheckoutPage extends StatefulWidget{
  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  bool isSelectedDebit = false;
  bool isSelectedCredit = false;
  bool isCheckedOut = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp (
      title: 'Checkout',
        home: Scaffold (
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.blueGrey,
            title: Text('Checkout'),
            actions: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: IconButton(
                  icon: Icon(Icons.home),
                  color: Colors.white,
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
          body: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  FilterChip (
                    label: Text('Debit'),
                    selected: isSelectedDebit,
                    onSelected: (value) {
                      setState(() {
                        isSelectedDebit = !isSelectedDebit;
                      });
                    },
                  ),
                  FilterChip (
                    label: Text('Credit'),
                    selected: isSelectedCredit,
                    onSelected: (value) {
                      setState(() {
                        isSelectedCredit = !isSelectedCredit;
                      });
                    },
                  ),
                ]
              ),
              Column (
                children: [
                  new ListTile (
                    leading: const Icon(Icons.person),
                    title: new TextField (
                      decoration: new InputDecoration(
                        hintText : 'Cardholder Name',
                      ),
                    ),
                  ),
                  new ListTile (
                    leading: const Icon(Icons.credit_card),
                    title: new TextField(
                      decoration: new InputDecoration(
                        hintText: 'Card Number',
                      ),
                    ),
                  ),
                  new ListTile(
                    title: new TextField(
                      decoration: new InputDecoration(
                        hintText: 'CVV',
                      ),
                    ),
                  ),
                  new ListTile(
                    title: new TextField(
                      decoration: new InputDecoration(
                        hintText: 'EXP 00/00',
                      ),
                    ),
                  ),
                  ActionChip(
                    avatar:
                      isCheckedOut ? CircularProgressIndicator () : null,
                    label: Text(
                      '${isCheckedOut ? 'Processing...' : 'Checkout'}'),
                    labelStyle: TextStyle(color: Colors.black),
                    onPressed: () {
                        setState(() {
                          isCheckedOut = !isCheckedOut;
                        });
                      },
                    ),

                ]
              ),
            ]
          ),
        )
    );

  }
}


class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool isSelectedNuts = false;
  bool isSelectedMilk = false;
  bool isSelectedLatex = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Profile Page',
        home: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            backgroundColor: Colors.blueGrey,
            title: Text('Profile Page'),
            actions: [
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                child: IconButton(
                  icon: Icon(Icons.home),
                  color: Colors.white,
                  onPressed: () {
                    Navigator.pop(context);
                    },
                ),
              ),
            ],
          ),
          body: ListView(
              children: [
                Image.asset(
                  'anthony.JPG',
                  width: 600,
                  height: 200,
                  fit: BoxFit.cover,
                ),
                Column(
                  children: [
                    Text('                                                                                       ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.black,backgroundColor: Colors.blueGrey)),
                    Text('                 First Name:   Anthony                            ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.black,backgroundColor: Colors.blueGrey)),
                    Text('                 Last Name:   Palermo                             ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.black,backgroundColor: Colors.blueGrey)),
                    Text('                 D.O.B.:   September 5, 1998                      ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.black,backgroundColor: Colors.blueGrey)),
                    Text('                 Phone #:   (123)456- 789                         ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.black,backgroundColor: Colors.blueGrey)),
                    Text('                 Allergies(Check all that apply)                        ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.black,backgroundColor: Colors.blueGrey)),
                    Text('                                                                                       ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400, color: Colors.black,backgroundColor: Colors.blueGrey)),
                    Container(
                      color: Colors.blueGrey,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,

                          children: [
                            FilterChip(
                              label: Text('Nuts'),
                              selected: isSelectedNuts,
                              onSelected: (value) {
                                setState(() {
                                  isSelectedNuts = !isSelectedNuts;
                                });
                              },
                            ),
                            FilterChip(
                              label: Text('Milk'),
                              selected: isSelectedMilk,
                              onSelected: (value) {
                                setState(() {
                                  isSelectedMilk = !isSelectedMilk;
                                });
                              },
                            ),
                            FilterChip(
                              label: Text('Latex'),
                              selected: isSelectedLatex,
                              onSelected: (value) {
                                setState(() {
                                  isSelectedLatex = !isSelectedLatex;
                                });
                              },
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ]
          ),
        )
    );
  }
}
